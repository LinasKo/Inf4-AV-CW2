function plotpcl_mesh(pcl)
	x  = pcl(:, :, 4);
	y  = pcl(:, :, 5);
	z  = pcl(:, :, 6);
	XYZ = [x(:) y(:) z(:)];

	ang_x = -90;
	ang_y = 90;
	ang_z = 180;

	Rtx = [1          0            0
	       0 cosd(ang_x) -sind(ang_x)
	       0 sind(ang_x)  cosd(ang_x)];

	Rty = [ cosd(ang_y)  0 sind(ang_y)
		0            1           0
	       -sind(ang_y)  0 cosd(ang_y)];

	Rtz = [cosd(ang_z) -sind(ang_z) 0
	       sind(ang_z)  cosd(ang_z) 0
	       0            0           1];

	R = Rtx*Rty*Rtz;
	XYZ = (R*XYZ')';
	[m,n,~] = size(pcl);

	X = reshape(XYZ(:,1), [m n]);
	Y = reshape(XYZ(:,2), [m n]);
	Z = reshape(XYZ(:,3), [m n]);
	warp(X,Y,Z, pcl(:,:,1:3)/255);
	hold on
	ylabel('X')
	zlabel('Y')
	xlabel('Depth')
	set(gca,'zdir','reverse')
end
