function res = get_nan_num(x, y, data, region_size)
    % Checks a <region_size> x <region_size> region around a point <x, y> for
    % NaN values. Returns the number of NaN's found.
    
    % Given region of 500 and boundary of 300, x = [301, 901]
    
    % figure out bounds
    x1 = x - ceil((region_size - 1) / 2);
    y1 = y - ceil((region_size - 1) / 2);
    x2 = x + floor((region_size - 1) / 2);
    y2 = y + floor((region_size - 1) / 2);

    % find number of outliers
    data1 = isnan(data(x1:x2, y1:y2, 1));
    data2 = isnan(data(x1:x2, y1:y2, 2));
    data3 = isnan(data(x1:x2, y1:y2, 3));
    
    final = data1 | data2 | data3;
    res = sum(sum(data1));
    return
end