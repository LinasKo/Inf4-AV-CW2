zs = load('hlzstatic.mat');
zs = zs.highlightimages;

zm = load('hlzmotion.mat');
zm = zm.highlightimages;


xm = load('hlxmotion.mat');
xm = xm.highlighted;


% ym = load('hlymotion.mat');
% ym = ym.highlightimages;


for ii = 1:11
    im = zs(:,:,:,ii);
    imshow(im);
    pause(1);    
end

for ii = 1:11
    im = zm(:,:,:,ii);
    imshow(im);
    pause(1);    
end

for ii = 1:11
    im = xm(:,:,:,ii);
    imshow(im);
    pause(1);    
end


% for ii = 1:11
%     im = ym(:,:,:,ii);
%     imshow(im);
%     pause(1);    
% end