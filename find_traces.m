function [shift] = find_traces( x1, x2, y1, y2, first, varargin )
%FIND_MATCHES Summary of this function goes here
%   Detailed explanation goes here
images = {};
points = {};
features = {};
index_pairs = {};
matched_points = {};

% Lowering MinContrast causes "Index exceeds matrix dimensions."
detector1 = @(x) detectFASTFeatures(x, 'MinContrast', 0.02, 'MinQuality', 0.00001);
detector2 = @(x) detectSURFFeatures(x, 'MetricThreshold', 100.0);

images{1} = first(x1:x2, y1:y2);

points{1} = detector1(images{1});
[feats, valids] = extractFeatures(images{1}, points{1});
features{1} = feats;
matched_points{1} = valids;

for index = 1:length(varargin)
    images{index+1} = varargin{index}(x1:x2, y1:y2);
    points{index+1} = detector1(images{index+1});
    [feats, valids] = extractFeatures(images{index+1}, points{index});
    
    index_pair = matchFeatures(features{index}, feats);
    index_pairs{index} = index_pair;
    
    matched_points{index} = matched_points{index}(index_pair(:, 1));
    matched_points{index+1} = valids(index_pair(:, 2));
    
    feats_old = features{index};
    feats_old.Features = feats_old.Features(index_pair(:, 1));
    feats_old.NumFeatures = length(feats_old.Features);
    
    features{index} = 1
    features{index+1} = feats(index_pair(:, 2));
    
    % Clear up mismatches
    % distances = sum((matched_points1.Location - matched_points2.Location).^2, 2).^0.5;
    % avg_distances = mean(distances);
    % valid = distances < 1.25 * avg_distances & distances > 0.9 * avg_distances;
    % matched_points1 = matched_points1(valid);
    % matched_points2 = matched_points2(valid);
    
    % mean_shift = mean(matched_points1.Location - matched_points2.Location);
    % shift = [shift; mean_shift];
    
    disp(size(matched_points));
end


% figure; showMatchedFeatures(images{1}, images{2}, matched_points{1}, matched_points{2});

return
end

