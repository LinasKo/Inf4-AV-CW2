function colourimage = generatecolourimage(rms, e)

redfilter = e < -rms;

yellowfilter = (-rms <= e) & (e < 0);

greenfilter = (0 <= e) & (e < rms);

bluefilter = (rms < e);

redchannel = (redfilter * 255) + (yellowfilter * 255);
greenchannel = (greenfilter * 255) + (yellowfilter * 255);
bluechannel = bluefilter * 255;

colourimage = cat(3,redchannel,greenchannel,bluechannel);

end