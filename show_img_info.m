function res = show_img_info(filename)
    plane = load(filename);
    
    x1 = 350;
    x2 = 850; 
    y1 = 350;
    y2 = 850;
    
    colormap('Gray');
    
    % Display original image
    newimage = highlight(plane.Img, x1, x2, y1, y2);
    subplot(2,3,1), imagesc(newimage);
    
    % Display X for region
    X = plane.XYZ(x1:x2, y1:y2, 1);
    subplot(2,3,2), imagesc(X);
    
    % Display Y for region
    Y = plane.XYZ(x1:x2, y1:y2, 2);
    subplot(2,3,3), imagesc(Y);
    
    % Display Z for region
    Z = plane.XYZ(x1:x2, y1:y2, 3);
    subplot(2,3,4), imagesc(Z);
    
    xyz = plane.XYZ;
    [rms_value, avgz, e] = rms(xyz, x1, x2, y1, y2);
    
    % Colormap
    color = generatecolourimage(rms_value, e);
    subplot(2,3,5), imshow(color);
    
    % RMS
    rms_value
    
end