function [shift, scale] = find_matches( x1, x2, y1, y2, first, varargin )
%FIND_MATCHES Summary of this function goes here
%   Detailed explanation goes here
images = {};
points = {};
features = {};
valid_points = {};
index_pairs = {};
matched_points = {};
shift = [];
scale = [];

% Lowering MinContrast causes "Index exceeds matrix dimensions."
detector1 = @(x) detectFASTFeatures(x, 'MinContrast', 0.02, 'MinQuality', 0.00001);
detector2 = @(x) detectSURFFeatures(x, 'MetricThreshold', 10, 'NumOctaves', 2, 'NumScaleLevels', 3);

images{1} = first(x1:x2, y1:y2);

points{1} = detector1(images{1});
[feats, valids] = extractFeatures(images{1}, points{1});
features{1} = feats;
valid_points{1} = valids;

for index = 1:length(varargin)
    images{index+1} = varargin{index}(x1:x2, y1:y2);
    points{index+1} = detector2(images{index+1});
    
    [feats, valids] = extractFeatures(images{index+1}, points{index});
    features{index+1} = feats;
    valid_points{index+1} = valids;
    
    if length(feats.Features) > 20000
        index_pair = matchFeatures(features{index}, features{index+1}, 'Method', 'Approximate');
    else
        index_pair = matchFeatures(features{index}, features{index+1});
    end
    index_pairs{index} = index_pair;
    
    matched_points1 = valid_points{index}(index_pair(:, 1));
    matched_points2 = valid_points{index+1}(index_pair(:, 2));
    
    % Clear up mismatches
    distances = sum((matched_points1.Location - matched_points2.Location).^2, 2).^0.5;
    avg_distances = mean(distances);
    valid = distances < 2.0 * avg_distances;
    % matched_points1 = matched_points1(valid);
    % matched_points2 = matched_points2(valid);
    
    mean_shift = median(matched_points1.Location - matched_points2.Location);
    shift = [shift; mean_shift];

    % showMatchedFeatures(images{1}, images{2}, matched_points1, matched_points2);

    tform = fitgeotrans(matched_points1.Location, matched_points2.Location, 'nonreflectivesimilarity');
    
    tformInv = invert(tform);
    Tinv = tformInv.T;
    ss = Tinv(2,1);
    sc = Tinv(1,1);
    scale_recovered = sqrt(ss*ss + sc*sc);
    scale = [scale, scale_recovered];
end

% figure; showMatchedFeatures(images{1}, images{2}, matched_points{1}, matched_points{2});

return
end