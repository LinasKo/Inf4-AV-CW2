
% calculate RMS error & x value of the visible point for ymotion

% manual selection of image slice
% takes slice from left hand side, as final images in set have large areas
% on right side which are not part of the textured plane

y1 = 200;
y2 = 1000;
x1 = 50;
x2 = 200;

y1_displ = 200;
y2_displ = 1000;
x1_displ = 50;
x2_displ = 200;


%empty list for RMS and average z values
rmslist = zeros(11,1);
avgzlist = zeros(11,1);

total_shift = [0 0];

xs = [];

% hfig = figure;
% set(hfig,'position',pos.*[.5 1 2 1])

%for each of the 11 images
for ii = 1:11
    if ii == 7
        y1 = 200;
        y2 = 1000;
        x1 = 50;
        x2 = 200;
    end
    
    %create the string to load the dataset
    loadstr = [sprintf('YMOTION/planey_%02d', 1 + ii * 5), '.mat'];
    
    %load the dataset
    plane = load(loadstr);
    
    %get the xyz values
    xyz = plane.XYZ;
    
    if ii > 1
        % find scene motion
        shift = find_matches( 1, 1200, 1, 1200, prev_plane.Img, plane.Img);
        total_shift = total_shift + shift;
        % disp(shift);
        x1 = round(x1 - shift(2));
        x2 = round(x2 - shift(2));
        y1 = round(y1 - shift(1));
        y2 = round(y2 - shift(1));
        
        y1_displ = round(y1_displ - shift(2));
        y2_displ = round(y2_displ - shift(2));
        x1_displ = round(x1_displ - shift(1));
        x2_displ = round(x2_displ - shift(1));
    end
    
    xs = [xs, round((y2_displ + y1_displ) / 2)];
    
    % check how many nan's we are getting.
    nans = get_nan_num_area(x1,x2,y1,y2,plane.Img);
    disp(sprintf('NaNs found for image "planey_%02d.mat: %d', 1 + ii * 5, nans));
    
    %calculate the RMS and avg. z values
    [rmsout, avgz, e] = rms(xyz,x1,x2,y1,y2);

    newimage = highlight(plane.Img, x1, x2, y1, y2);
    colour = generatecolourimage(rmsout, e);
    
    subplot(2,2,1), imagesc(newimage);
    subplot(2,2,[3 4]), imagesc(colour);
    axis image;
    pause(0.00000001);
    
    %append these onto their respective lists
    rmslist(ii) = rmsout;
    avgzlist(ii) = avgz;
    
    % store previous plane for use in next loop to estimate scene motion
    prev_plane = plane;
    
    numoutliers(ii) = sum(sum(abs(e) > (5*rmsout)));
end

disp(xs);
disp(total_shift);
subplot(2,2,2); plot(xs,rmslist);
ylabel('RMS error');
xlabel('Y scene position');
