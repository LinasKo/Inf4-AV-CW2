
% calculate RMS error & average z values for zstatic

% manual selection of image slice
% takes slice from left hand side, as final images in set have large areas
% on right side which are not part of the textured plane

x1 = 400;
x2 = 900;
y1 = 400;
y2 = 900;

total_shift = [0 0];

%empty list for RMS and average z values
rmslist = zeros(11,1);
avgzlist = zeros(11,1);
numoutliers = zeros(11,1);

%for each of the 11 images
for ii = 1:11

    %create the string to load the dataset
    loadstr = ['ZSTATIC/plane_',int2str(ii),'.mat'];
    
    %load the dataset
    plane = load(loadstr);
    
    %get the xyz values
    xyz = plane.XYZ;
    
    if ii > 1
        %find scene motion
        [shift, scale] = find_scales( 1, 1200, 1, 1200, prev_plane.Img, plane.Img);
        total_shift = total_shift + shift;

        if abs(scale) > 0.75 && abs(shift(1)) < 100 && abs(shift(2)) 
            scale_diff_x = (scale - 1.0) * (x2 - x1) / 2;
            scale_diff_y = (scale - 1.0) * (y2 - y1) / 2;
            
            x1 = round((x1 - shift(2)) - scale_diff_x);
            x2 = round((x2 - shift(2)) + scale_diff_x);
            y1 = round((y1 - shift(1)) - scale_diff_y);
            y2 = round((y2 - shift(1)) + scale_diff_y);
        end
    end
    
    %check how many nan's we are getting.
    nans = get_nan_num_area(x1,x2,y1,y2,plane.Img);
    disp(sprintf('NaNs found for image "planez_%02d.mat: %d"', ii, nans));

    %calculate the RMS and avg. z values
    [rmsout, avgz,e] = rms(xyz,x1,x2,y1,y2);
    
    newimage = highlight(plane.Img, x1, x2, y1, y2);
    colour = generatecolourimage(rmsout, e);
    
    subplot(1,2,1), imagesc(newimage);
    axis image;
    subplot(1,2,2), imagesc(colour);
    axis image;
    pause(0.00000001);
    
    if(ii == 6)
       e6 = e;
       rms6 = rmsout;
    end

    %append these onto their respective lists
    rmslist(ii) = rmsout;
    avgzlist(ii) = avgz;
    
    %store previous plane for use in next loop to estimate scene motion
    prev_plane = plane;
    
    %find number of outliers
    numoutliers(ii) = sum(sum(abs(e) > (5*rmsout))); 
    
    %find total area used
    pixels_used = (x2 - x1) * (y2 - y1);
    disp(sprintf('Number of pixels used: %d', pixels_used));
end

plot(avgzlist,rmslist);
ylabel('RMS error');
xlabel('Average Z value');

colourimage = generatecolourimage(rms6,e6);



