function nans = get_nan_num_area(x1, x2, y1, y2, data)
    % Checks a <region_size> x <region_size> region around a point <x, y> for
    % NaN values. Returns the number of NaN's found.
    
    % Given region of 500 and boundary of 300, x = [301, 901]

    % find number of outliers
    x1 = max(1, x1);
    y1 = max(1, x1);
    x2 = min(1200, x2);
    y2 = min(1200, y2);
    
    data1 = isnan(data(x1:x2, y1:y2, 1));
    %data2 = isnan(data(x1:x2, y1:y2, 2));
    %data3 = isnan(data(x1:x2, y1:y2, 3));
    
    %final = data1 | data2 | data3;
    nans = sum(sum(data1));
    return
end