
% calculate RMS error & average z values for zstatic

% manual selection of image slice
% takes slice from left hand side, as final images in set have large areas
% on right side which are not part of the textured plane
% 
% x1 = 450;
% x2 = 950;
% y1 = 150;
% y2 = 650;


x1 = 500;
x2 = 550;
y1 = 500;
y2 = 550;


%empty list for RMS and average z values
rmslist = zeros(11,1);
avgzlist = zeros(11,1);

%for each of the 11 images
for ii = 1:11

    %create the string to load the dataset
    loadstr = ['plane_',int2str(ii),'.mat'];
    
    %load the dataset
    plane = load(loadstr);
    
    %get the xyz values
    xyz = plane.XYZ;

    %calculate the RMS and avg. z values
    [rmsout, avgz] = rms(xyz,x1,x2,y1,y2);

    %append these onto their respective lists
    rmslist(ii) = rmsout;
    avgzlist(ii) = avgz;

end

plot(avgzlist,rmslist);