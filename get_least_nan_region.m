function [ x1, x2, y1, y2, out ] = get_least_nan_region( data, region_size, bounds_size, max_tries )
    % An interface for finidng a regio with least missing values..
    % Given an image, find the region with the least outliers. Get the
    % region bounds, and the number of outliers.
    % :data:        (1200, 1200, 6) image. Should work with images with less
    %               channels, as long as it's >= 3. Also different size will
    %               work. 
    % :region_size: the edge length of region. Assuming square regions.
    % :bounds_size: size of the bounds on the sides that will not be
    %               touched.
    % :max_tries:   max number of times to try to find the least outliers.
    %               If no region with 0 is detected, minimum-outlier region
    %               is returned.
    
    if(numel(data) == 1)
        XYZ = data.XYZ;
    else
        XYZ = data;
    end
    
    [x1, x2, y1, y2, out] = spiraling_outlier_search(XYZ, region_size, bounds_size, max_tries);
    %disp([x1, x2, y1, y2, out]);

    return
end


function [x1, x2, y1, y2, min_outliers] = spiraling_outlier_search(xyz, region_size, bounds_size, max_tries)
    % Goes in a spiral, checking for outliers, at most <max_tries> times.
    % Returns region bounds (inclusive) and outliers found.
    [x_dim, y_dim, channels] = size(xyz);
%     x = ceil(x_dim/2)+1;
%     y = ceil(y_dim/2)+1;

    %manually selecting starting location
    x = 850;
    y = 251;

    min_x = -1;
    min_y = -1;
    min_outliers = region_size * region_size;
    
    turn = true;  % Is it X's turn to move? true for X, false for Y.
    total_step_x = 1;
    total_step_y = 1;
    step_so_far_x = 0;
    step_so_far_y = 0;
    mult_x = 1;
    mult_y = 1;
    
    while max_tries > 0

        out = get_nan_num(x, y, data, region_size);

        if (out < min_outliers)
            min_outliers = out;
            min_x = x;
            min_y = y;
            if out == 0
                break
            end
        end
        
        % Move in a spiral form the initial point. Ask me if this is
        % confusing.
        if turn
            x = x + mult_x;
            step_so_far_x = step_so_far_x + 1;
            if step_so_far_x == total_step_x
                % turn a corner
                turn = ~turn;
                step_so_far_x = 0;
                total_step_x = total_step_x + 1;
                mult_x = mult_x * -1;
            end
        else
            y = y + mult_y;
            step_so_far_y = step_so_far_y + 1;
            
            if step_so_far_y == total_step_y
                % turn a corner
                turn = ~turn;
                step_so_far_y = 0;
                total_step_y = total_step_y + 1;
                mult_y = mult_y * -1;
            end
        end  
        
        max_tries = max_tries - 1;
    end
    
    x1 = min_x - ceil((region_size - 1) / 2);
    y1 = min_y - ceil((region_size - 1) / 2);
    x2 = min_x + floor((region_size - 1) / 2);
    y2 = min_y + floor((region_size - 1) / 2);
    return
end

