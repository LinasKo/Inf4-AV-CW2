function [rms, avgz, e] = rms(xyz, x1, x2, y1, y2)
% xyz is 1200X1200x3 XYZ coords
% x1 and x2 are range of pixels e.g 100-199
% y1 & y2 are the same

%total number of pixels
n = (x2 +1 - x1) * (y2 +1 -y1);

% want to generate a flat plane such that the distance between each point
% and the plane minimizes the RMS error

%xyzslice is {d} vector in handout
xyzslice = xyz(x1:x2, y1:y2, :);

% average of all of the z values in the section
avgz = mean(mean(xyzslice(:,:,3)));

%reshapes the square matrix into a Nx3
xyzslice = reshape(xyzslice,n,3);

%fits a plane to the z values
[plane,fit] = fitplane(xyzslice);

e = plane' * cat(2,xyzslice,ones(n,1))';
%root mean square error
rms = sqrt(sum(e .^2) / n);

e = reshape(e,(x2+1-x1),(y2+1-y1));


% %my old code, doesnt work for large matrices
% xyzslice = cat(2,xyzslice,ones(n,1))';
% 
% %single value decomposition
% %S is single values
% %want left singular vector with lowest sv
% 
% [U,S,~] = svd(xyzslice);
% 
% S = S(:,1:4);
% mindimension = find(max(S) == min(max(S)));
% 
% % p vector
% p = U(:,mindimension);
% % q vector
% q = p ./ sqrt(sum(p(1:3) .^2));
% % e vector
% e = q' * xyzslice;
% %root mean square error
% rms = sqrt(sum(e .^2) / n);

end