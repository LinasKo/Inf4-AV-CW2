
% calculate RMS error & x value of the visible point for xmotion

% manual selection of image slice
% takes slice from left hand side, as final images in set have large areas
% on right side which are not part of the textured plane

y1 = 50;
y2 = 150;
x1 = 200;
x2 = 1000;


%empty list for RMS and average z values
rmslist = zeros(11,1);
avgzlist = zeros(11,1);

total_shift = [0 0];

ys = [];

%for each of the 11 images
for ii = 1:11
    %create the string to load the dataset
    loadstr = [sprintf('XMOTION/planex_%04d', ii * 4), '.mat'];
    
    %load the dataset
    plane = load(loadstr);
    
    %get the xyz values
    xyz = plane.XYZ;
    
    if ii > 1
        % find scene motion
        shift = find_matches( 1, 1200, 1, 1200, prev_plane.Img, plane.Img);
        total_shift = total_shift + shift;
        % disp(shift);
        x1 = round(x1 - shift(2));
        x2 = round(x2 - shift(2));
        y1 = round(y1 - shift(1));
        y2 = round(y2 - shift(1));    
    end
    
   ys = [ys, round((y2 + y1) / 2)];
    
    % check how many nan's we are getting.
    nans = get_nan_num_area(x1,x2,y1,y2,plane.Img);
    disp(sprintf('NaNs found for image "planez_%04d.mat: %d', ii * 4, nans));
    
    %calculate the RMS and avg. z values
    [rmsout, avgz, e] = rms(xyz,x1,x2,y1,y2);

    newimage = highlight(plane.Img, x1, x2, y1, y2);
    colour = generatecolourimage(rmsout, e);
    
    subplot(2,2,1); imagesc(newimage);
    subplot(2,2,[2 4]); imagesc(colour);
    axis image;
    pause(0.00000001);
    
    %append these onto their respective lists
    rmslist(ii) = rmsout;
    avgzlist(ii) = avgz;
    
    % store previous plane for use in next loop to estimate scene motion
    prev_plane = plane;
    
    numoutliers(ii) = sum(sum(abs(e) > (5*rmsout)));
end

disp(ys);
disp(total_shift);
subplot(2,2,3); plot(ys,rmslist);
ylabel('RMS error');
xlabel('X scene position');
