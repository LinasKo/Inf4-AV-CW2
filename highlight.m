function imageout = highlight(imagein, x1,x2,y1,y2)

m1 = 255 * (ones(size(imagein)));
m2 = m1 .* imagein;

m2 = uint8(round(m2));

redlayer = m2;
redlayer(x1-3:x1+3,y1:y2) = 255;
redlayer(x2-3:x2+3,y1:y2) = 255;
redlayer(x1:x2,y1-3:y1+3) = 255;
redlayer(x1:x2,y2-3:y2+3) = 255;

greenlayer = m2;
greenlayer(x1-3:x1+3,y1:y2) = 0;
greenlayer(x2-3:x2+3,y1:y2) = 0;
greenlayer(x1:x2,y1-3:y1+3) = 0;
greenlayer(x1:x2,y2-3:y2+3) = 0;

bluelayer = m2;
bluelayer(x1-3:x1+3,y1:y2) = 0;
bluelayer(x2-3:x2+3,y1:y2) = 0;
bluelayer(x1:x2,y1-3:y1+3) = 0;
bluelayer(x1:x2,y2-3:y2+3) = 0;


imageout = cat(3,redlayer,greenlayer,bluelayer);


end