function playground()
% Place to try out new things
% [DATA] = load_pcl_mat('ZSTATIC/plane', 4);
% imwrite(DATA(:,:,4), 'out_x.png');
% imwrite(DATA(:,:,5), 'out_y.png');
% imwrite(DATA(:,:,6), 'out_z.png');
% imwrite(DATA(:,:,1), 'out_grey.png');

% plotpcl_mesh(DATA);

% disp(size(isnan(DATA)));
% [x y] = find(isnan(DATA(:,:,1:2)));
% disp([x y]);

% for i = 251:351
%     out = get_outlier_num(i, 951, DATA, -1, 500);
%     if out ~= 0
%         disp([i, 255, out]);
%     end
% end

x1 = 350;
x2 = 850;
y1 = 350;
y2 = 850;

for ii = 1:2
    %create the string to load the dataset
    loadstr = [sprintf('XMOTION/planex_%04d', ii * 4), '.mat']
    % loadstr = [sprintf('ZMOTION/planez_%02d.mat', 1 + (ii - 1) * 3)];
    % loadstr = ['ZSTATIC/plane_',int2str(ii),'.mat'];
    
    %load the dataset
    plane = load(loadstr);
    
    if ii > 1
        [shift_surf, scales] = find_scales( 1, 1200, 1, 1200, prev_plane.Img, plane.Img);
        disp(shift_surf);
        % shift_fast = find_matches( 1, 1200, 1, 1200, prev_plane.Img, plane.Img);
        % disp(shift_fast);
        
        % newimage = highlight(plane.Img, x1, x2, y1, y2);
        % imagesc(newimage); pause(0.00000001);
        
        disp(scales);
        scale_diff_x = 0;
        scale_diff_y = 0;
        if abs(scales) > 0.75
            scale_diff_x = (scales - 1.0) * (x2 - x1) / 2;
            scale_diff_y = (scales - 1.0) * (y2 - y1) / 2;
            
            x1 = round((x1 - shift_surf(2)) - scale_diff_x);
            x2 = round((x2 - shift_surf(2)) + scale_diff_x);
            y1 = round((y1 - shift_surf(1)) - scale_diff_y);
            y2 = round((y2 - shift_surf(1)) + scale_diff_y);
        end
    end
    
    prev_plane = plane;
end


end